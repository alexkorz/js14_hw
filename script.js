const changeThemeButton = document.getElementById("change-theme");
const stylesheet = document.querySelector('link:not([href="css/reset.css"])');


if(localStorage.getItem("theme") === "css/style-dark.css") {
  console.log('dark uploaded')
  stylesheet.setAttribute("href", "css/style-dark.css");
} else if(localStorage.getItem("theme") === "css/style-light.css"){
  console.log('light uploaded')
  stylesheet.setAttribute("href", "css/style-light.css");
}

changeThemeButton.addEventListener("click", changeTheme );

function changeTheme () {
  if(stylesheet.getAttribute("href") === "css/style-dark.css") {
    localStorage.setItem("theme", "css/style-light.css");
    stylesheet.setAttribute("href", "css/style-light.css");
  } else {
    localStorage.setItem("theme", "css/style-dark.css");
    stylesheet.setAttribute("href", "css/style-dark.css");
  }
}

